package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Transactions;
import com.example.demo.repository.*;

@ActiveProfiles("h2")
@SpringBootTest

public class MySQLTransactionRepositoryTest {

	@Autowired
	MySQLTransactionsRepository mySQLTransactionRepository;
		
	@Test
	public void testGetAllTransactions() {
		// 1. Any setup stuff
		
		// 2. Call the method under test
		List<Transactions> returnedList = mySQLTransactionRepository.getAllTransactions();
		
		// 3 Verify the results
		assertThat(returnedList).isNotNull();
		
		for(Transactions transaction: returnedList) {
			System.out.println("Transaction is: " + transaction.getTransaction_id());
		}
	}
}