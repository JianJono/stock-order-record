package com.example.demo.controller;

import com.example.demo.entities.*;
import java.util.Date;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testShouldReturnListSize3() throws Exception {
		// 1. setup stuff
		
		
		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks"))
							              .andDo(print())
							              .andExpect(status().isOk())
							              .andReturn();
		
		// 3. verify the results
		List<Transactions> transactions = new ObjectMapper().readValue(
											mvcResult.getResponse().getContentAsString(),
				                            new TypeReference<List<Transactions>>() { });
		
		assertThat(transactions.size()).isGreaterThan(0);
	}
	
	@Test
	public void testCreateTransaction() throws Exception {
		// 1. setup stuff
		String test_stock_ticker = "AAPL";
		String test_buy_or_sell = "BUY";
		int test_status_code = 0;
		int test_quantity = 10;
		//int test_transaction_id = 33;
		double test_price = 1000;
		
		Transactions testTransaction = new Transactions();
		testTransaction.setCustomer_id(1);
		//testTransaction.setTransaction_id(test_transaction_id);
        testTransaction.setBuy_or_sell(test_buy_or_sell);
        testTransaction.setStock_ticker(test_stock_ticker);
        testTransaction.setQuantity(test_quantity);
        testTransaction.setPrice(test_price);
        testTransaction.setStatus_code(test_status_code); 
        
        
        
		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testTransaction);
		
		System.out.println(requestJson);

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(post("/api/stocks/")
										  			.header("Content-Type", "application/json")
										  			.content(requestJson))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		Transactions transaction = new ObjectMapper().readValue(
				mvcResult.getResponse().getContentAsString(),
                new TypeReference<Transactions>() { });
        //assertThat(transaction.getTransaction_id((keyHolder.getKey().intValue())))
		//assertThat(transaction.getTransaction_id()).isEqualTo(test_transaction_id);
		assertThat(transaction.getStock_ticker()).isEqualTo(test_stock_ticker);
		assertThat(transaction.getQuantity()).isEqualTo(test_quantity);
		assertThat(transaction.getPrice()).isEqualTo(test_price);
		assertThat(transaction.getBuy_or_sell()).isEqualTo(test_buy_or_sell);
	
	}
	
	@Test
	public void testDeleteTransactions() throws Exception {
		// 1. setup stuff
		int testId = 2;

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(delete("/api/stocks/" + testId)
										  			.header("Content-Type", "application/json"))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		int numRows = new ObjectMapper().readValue(
				mvcResult.getResponse().getContentAsString(),
                new TypeReference<Integer>() { });

		assertThat(numRows).isEqualTo(1);
	}
	
	/*
	@Test
	public void testEditTransactions() throws Exception {
		// 1. setup stuff
		int testId = 1;
		double test_price = 10000 ;
		int test_quantity = 5;
		
		Transactions testTransactions = new Transactions();
		testTransactions.setTransaction_id(testId);
        testTransactions.setPrice();;
        testTransactions.setPhone(testPhone);
        
		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testTransactions);
		
		System.out.println(requestJson);

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(put("/api/stocks/")
										  			.header("Content-Type", "application/json")
										  			.content(requestJson))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		Transactions Transactions = new ObjectMapper().readValue(
				mvcResult.getResponse().getContentAsString(),
                new TypeReference<Transactions>() { });

		assertThat(Transactions.getName()).isEqualTo(testName);
		assertThat(Transactions.getPhone()).isEqualTo(testPhone);
		
		// verify the edit took place
		mvcResult = this.mockMvc.perform(get("/api/Transactionss/" +  testId))
	              .andDo(print())
	              .andExpect(status().isOk())
	              .andReturn();


		Transactions = new ObjectMapper().readValue(
					mvcResult.getResponse().getContentAsString(),
					new TypeReference<Transactions>() { });

		assertThat(Transactions.getId()).isEqualTo(testId);
		assertThat(Transactions.getName()).isEqualTo(testName);
		assertThat(Transactions.getPhone()).isEqualTo(testPhone);
	}
	
	*/
	
}