package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import com.example.demo.service.*;
import com.example.demo.repository.*;
import com.example.demo.entities.*;


@ActiveProfiles("h2")
@SpringBootTest
public class TransactionServiceTest {

	// Get a "real" shipperService from the spring container
	@Autowired
	StockService stockService;

	// Get a "mock" or "fake" shipper repository
	// from the spring container
	@MockBean
	TransactionRepository transactionRepository;
	
	@Test
	public void testGetShipper() {
		// 1. Setup stuff
		// create a shipper record for testing
		int testId = 5;
		String test_stock_ticker = "AAPL";
		String test_buy_or_sell = "BUY";
		int test_status_code = 0;
		int test_quantity = 10;
		double test_price = 1000;
		
		Transactions testTransaction = new Transactions();
        testTransaction.setBuy_or_sell(test_buy_or_sell);
        testTransaction.setStock_ticker(test_stock_ticker);
        testTransaction.setQuantity(test_quantity);
        testTransaction.setPrice(test_price);
        testTransaction.setStatus_code(test_status_code); 
		
		// tell the mock object what to do when
		// its getShipperById method is called
        List<Transactions> transactions;
		when(transactions = transactionRepository.getAllTransactions())
		.thenReturn(transactions);
		
		
		// 2. call class under test
		// call the getShipper method on the service, this will call
		// the getShipperById method on the the mock repository
		// the mock repository returns the testShipper
		// the service should return the same testShipper here
		// we verify this happens, so we know the service is behaving as expected
		List<Transactions> returnedTransactions = stockService.getAllTransactions();
		
		
		// 3. verify response
		assertThat(transactions).isEqualTo(returnedTransactions);
	}
	
	// Test for a "NotFound scenario"
	// NOTE: this test is unnecessary for this particular class.
	//		 It's only here to demonstrate how to generate and test for
	//       an exception in a test that uses mocking.
	
}