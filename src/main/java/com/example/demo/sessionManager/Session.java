package com.example.demo.sessionManager;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class Session {

	@Autowired 
	JdbcTemplate template;
	@SuppressWarnings("deprecation")
	public int authenticateUser(String userName, String password)
	{
		int sessionKey = 0 ;
		String sql = "SELECT password FROM customer_details WHERE customer_name = ";
		String pswd = (String) template.queryForObject(sql, new Object[]{userName}, String.class);
		
		Random rand = new Random();
		
		if(password.equals(pswd))
			sessionKey = rand.nextInt(10000) ;
		else 
			sessionKey = -1 ;
		
		return sessionKey;				
	}
		
}
