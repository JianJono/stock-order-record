/**
 * 
 */
package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Transactions;
import com.example.demo.repository.TransactionRepository;

@Service
public class StockService {

	@Autowired
	private TransactionRepository repository;


	public List<Transactions> getAllTransactions() {
		return repository.getAllTransactions();
	}
	
//	public List<Transactions> getTransactions(int id) {
//		return repository.getTransactionById(id);
//	}


	public Transactions addTransactions(Transactions transactions) {
		return repository.addTransactions(transactions);
	}

	public int deleteTransactions(int id) {
		return repository.deleteTransactions(id);
	}
	
	public int updateTransactions(Transactions transactions) {
		return repository.updateTransactions(transactions);
	}
	
}
