package com.example.demo.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Transactions;

@Repository
public class MySQLTransactionsRepository implements TransactionRepository{

	@Autowired
	JdbcTemplate template;

	@Override
	public List<Transactions> getAllTransactions() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM stock_orders";
		return template.query(sql, new TransactionsRowMapper());
	}

	@Override
	public Transactions addTransactions(Transactions transaction) {
		// TODO Auto-generated method stub
		KeyHolder keyHolder = new GeneratedKeyHolder();
		template.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO stock_orders(stock_ticker, customer_id, buy_or_sell, status_code,quantity,price) VALUES(?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, transaction.getStock_ticker());
				ps.setInt(2, transaction.getCustomer_id());
				ps.setString(3, transaction.getBuy_or_sell());
				ps.setInt(4, transaction.getStatus_code());
				ps.setInt(5, transaction.getQuantity());
				ps.setDouble(6, transaction.getPrice());

				return ps;
			}
		}, keyHolder);

		transaction.setTransaction_id((keyHolder.getKey().intValue()));
		return transaction;
	}

//	@Override
//	public List<Transactions> getTransactionById(int id) {
//		// TODO Auto-generated method stub
//		String sql = "SELECT Transaction FROM stock_orders where customer_id = "+id;
//		return template.query(sql, new TransactionsRowMapper());
//	}

	@Override
	public int deleteTransactions(int id) {
		// TODO Auto-generated method stub
		String sql1 ="Select status_code from stock_orders where transaction_id= "+id;
		int status = template.queryForObject(sql1, Integer.class);
		if(status<2)
		{
			String sql = "DELETE FROM stock_orders where transaction_id = ?";
			return template.update(sql,id);
		}
		else
		{
			if(status==2)
			{
				System.out.println("Cannot delete successful transaction! ");
				
			}
			else if(status==3)
			{
				System.out.println("Cannot delete failed transaction! ");
				
			}
			return 0;
			
		}

	}

	@Override
	public int updateTransactions(Transactions transactions) {
		// TODO Auto-generated method stub
		
		String sql1 ="Select status_code from stock_orders where transaction_id= "+transactions.getTransaction_id();
		int status = template.queryForObject(sql1, Integer.class);
		if(status!=2)
		{
			String sql = "update stock_orders set price = ?, quantity = ? where transaction_id = ?";
			return template.update(sql, transactions.getPrice(),transactions.getQuantity(),transactions.getTransaction_id());
		}
		else
		{
			if(status==2)
			{
				System.out.println("Cannot modify successful transaction! ");
				
			}
			return 0;
		}
		
	}
	
	
}

class TransactionsRowMapper implements RowMapper<Transactions>{

	@Override
	public Transactions mapRow(ResultSet rs, int rowNum) throws SQLException {
		System.out.println("in row mapper ");
		
		return new Transactions(rs.getInt("transaction_id"),
				rs.getString("stock_ticker"),
				rs.getString("buy_or_sell"),
				rs.getInt("status_code"),
				rs.getInt("quantity"),
		        rs.getDouble("price"),
				rs.getInt("customer_id"),
				rs.getTimestamp("order_placed_on"),
				rs.getTimestamp("order_fulfilled_on")
				);
	}
	
}
