package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Transactions;

@Component
public interface TransactionRepository {

	public List<Transactions> getAllTransactions();
	public Transactions addTransactions(Transactions Transactions);
	//public List<Transactions> getTransactionById(int id);
	public int deleteTransactions(int id);
	public int updateTransactions(Transactions transactions);
	
	
//	public Transactions getTransactions(int id); 
//	public int deleteTransactions(int id);


}
