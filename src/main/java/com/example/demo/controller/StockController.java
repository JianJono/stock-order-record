/**
 * 
 */
package com.example.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Transactions;
import com.example.demo.service.StockService;

@CrossOrigin("*")
@RestController
@RequestMapping("api/stocks")
public class StockController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

	@Autowired
	private StockService service;
	
	@GetMapping
	public List<Transactions> getAllTransactions() {
		LOG.debug("getAllTransactions");
		return service.getAllTransactions();
	}

//	@GetMapping(value = "/{id}")
//	public List<Transactions> getTransactions(@PathVariable("id") int id) {
//		LOG.debug("getTransactions, id=[" + id + "]");
//		return service.getTransactions(id);
//	}

	@PostMapping
	public Transactions addTransactions(@RequestBody Transactions transactions) {
		return service.addTransactions(transactions);
	}

	@PutMapping(value = "/{id}")
	public int updateTransactions(@RequestBody Transactions transactions) {
		LOG.debug("update Transaction");
		return service.updateTransactions(transactions);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteTransactions(@PathVariable int id) {
		LOG.debug("delete Transaction");
		return service.deleteTransactions(id);
	}
}
