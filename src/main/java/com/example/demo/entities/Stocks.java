package com.example.demo.entities;

public class Stocks {
	
	private String stock_ticker;
	private int price;
	private int volume;
	
	public Stocks() 
	{
		
	}
	
	public Stocks(String stock_ticker, int price, int volume) {
		super();
		this.stock_ticker = stock_ticker;
		this.price = price;
		this.volume = volume;
	}

	public String getStock_ticker() {
		return stock_ticker;
	}

	public void setStock_ticker(String stock_ticker) {
		this.stock_ticker = stock_ticker;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	

}
