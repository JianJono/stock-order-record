package com.example.demo.entities;

public class Customers {
	
	private int customer_id;
	private String customer_name;
	private String phone;
	private String password ;
	
	public Customers() {}
	
	

	public Customers(int customer_id, String customer_name, String phone) {
		super();
		this.customer_id = customer_id;
		this.customer_name = customer_name;
		this.phone = phone;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
