package com.example.demo.entities;

import java.util.Date;

public class Transactions {
 
	private int transaction_id;
	private String stock_ticker;
	private String buy_or_sell;
	private int status_code;
	private int quantity;
	private double price;
	private int customer_id;
	private Date order_placed_on ;
	private Date order_fulfilled_on ;
	
	
	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}



	
	public Transactions() {}
	
	
//	public Transactions(int transaction_id, String stock_ticker, String buy_or_sell, int status_code, int customer_id) {
//		super();
//		this.transaction_id = transaction_id;
//		this.stock_ticker = stock_ticker;
//		this.buy_or_sell = buy_or_sell;
//		this.status_code = status_code;
//		this.customer_id = customer_id;
//	}

	

	public int getTransaction_id() {
		return transaction_id;
	}


	public Transactions(int transaction_id, String stock_ticker, String buy_or_sell, int status_code, int quantity,
		double price, int customer_id, Date order_placed_on, Date order_fulfilled_on) {
	super();
	this.transaction_id = transaction_id;
	this.stock_ticker = stock_ticker;
	this.buy_or_sell = buy_or_sell;
	this.status_code = status_code;
	this.quantity = quantity;
	this.price = price;
	this.customer_id = customer_id;
	this.order_placed_on = order_placed_on;
	this.order_fulfilled_on = order_fulfilled_on;
}


	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}


	public String getStock_ticker() {
		return stock_ticker;
	}


	public void setStock_ticker(String stock_ticker) {
		this.stock_ticker = stock_ticker;
	}


	public String getBuy_or_sell() {
		return buy_or_sell;
	}


	public void setBuy_or_sell(String buy_or_sell) {
		this.buy_or_sell = buy_or_sell;
	}


	public int getStatus_code() {
		return status_code;
	}


	public void setStatus_code(int status_code) {
		this.status_code = status_code;
	}


	public int getCustomer_id() {
		return customer_id;
	}


	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}


	public Date getOrder_placed_on() {
		return order_placed_on;
	}


	public void setOrder_placed_on(Date order_placed_on) {
		this.order_placed_on = order_placed_on;
	}


	public Date getOrder_fulfilled_on() {
		return order_fulfilled_on;
	}


	public void setOrder_fulfilled_on(Date order_fulfilled_on) {
		this.order_fulfilled_on = order_fulfilled_on;
	}
	
	
}
