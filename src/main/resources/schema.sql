
-- create table stock_details(stock_ticker varchar(15) primary key, stock_price int, stock_volume int);
create table if not exists customer_details(customer_id int auto_increment primary key, customer_name varchar(50), phone varchar(15), password varchar(15));
create table if not exists stock_orders(transaction_ID int auto_increment primary key,
	          stock_ticker varchar(15),
			  order_placed_on timestamp,
			  order_fulfilled_on timestamp on update now(),
			  customer_id int,
			  buy_or_sell varchar(20),
			  quantity int,
			  status_code int,
			  price double,
			  FOREIGN key (customer_id) REFERENCES customer_details(customer_id));


-- insert into stock_details values('TCS',3463,43);
-- insert into stock_details values('L&T',1668,54);
-- insert into stock_details values('RIL',2145,58);
-- insert into stock_details values('BPCL',454,71);
-- insert into stock_details values('HDFC',2704,32);
-- insert into stock_details values('ITC',211,188);
-- insert into stock_details values('SBI',431,209);
-- insert into stock_details values('HUL',2400,10);
-- insert into stock_details values('UPL',779,1);
-- insert into stock_details values('ONGC',116,103);


insert into customer_details(customer_name,phone,password) values('Prachi','1234', 'prachi');
-- insert into customer_details(customer_name,phone,password) values('Bhavya','4321','bhavya');
-- insert into customer_details(customer_name,phone,password) values('Josh','3421','jash');
-- insert into customer_details(customer_name,phone,password) values('Jash','99887','jash');
-- insert into customer_details(customer_name,phone,password) values('Ravi','4567','ravi');
-- insert into customer_details(customer_name,phone,password) values('Aditya','7888','aditya');


insert into stock_orders(customer_id,stock_ticker,status_code,quantity,price,buy_or_sell) values(1, 'AMZN',0,20,100,'buy');
insert into stock_orders(customer_id,stock_ticker,status_code,quantity,price,buy_or_sell) values(1, 'TCS',0,2,876,'sell');
insert into stock_orders(customer_id,stock_ticker,status_code,quantity,price,buy_or_sell) values(1, 'INFY',0,40,80,'buy');
